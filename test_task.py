# Program is written by Lapshova Marina
# 2017
import argparse
import logging

from os.path import exists, abspath, dirname, join


def create_logger():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        '%(asctime)s - %(pathname)s - %(levelname)s - %(message)s'
    )
    file_h = logging.FileHandler(join(abspath(dirname(__file__)), 'log.txt'))
    file_h.setLevel(logging.DEBUG)
    file_h.setFormatter(formatter)

    logger.addHandler(file_h)
    return logger


def get_args():
    """
    It parses arguments from command line
    :return: path to file config and path to text file
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("path_config", type=str, help="Path to config file")
    parser.add_argument(
        "path_text", type=str, help="Path to text file for changes"
    )
    return parser.parse_args()


def get_dict_changes(config):
    """
    It gets change table from config file
    :param config: Path to config file
    :return: Change table
    """
    change_table = {}
    for line in config:
        try:
            key, value = line.strip().split('=')
            change_table[key] = value
        except Exception as err:
            print("Error in config file's format: value1=value2.\n'{}'".format(line.strip()))
            logger.exception(err)
            continue
    return change_table


def change_matches_in_file(text, change_table):
    """
    It changes file according to dictionary
    :param path_text: Path to text file
    :param change_table: Changes are made according to this dictionary
    """
    for key, value in change_table.items():
        text = text.replace(key, value)
    return text


logger = create_logger()
args = get_args()
for i in ('path_config', 'path_text'):
    path = getattr(args, i)
    if not exists(path):
        print("File '{}' don't exist".format(path))
        exit()

try:
    with open(args.path_config, encoding='utf-8') as config, open(
            args.path_text, encoding='utf-8') as text:
        change_table = get_dict_changes(config)
        new_text = change_matches_in_file(text.read(), change_table)
except OSError as err:
    print("File can't be opened. See more in log.txt ")
    logger.exception(err)
except UnicodeError as err:
    print("Files must be in encoding UTF-8. See more in log.txt")
    logger.exception(err)
else:
    try:
        with open(args.path_text, 'w', encoding='utf-8') as f:
            f.write(new_text)
    except OSError as err:
        print("File {} can't be opened!".format(args.path_text))
        logger.exception(err)
    print(new_text[::-1])
