# Description #
This program automatically changes text according to dictionary.

## Requirements ##
python >= 3.5

## How to use ##
Start program via command line with arguments:

* config file
* text file

`python test_task.py <path_to_config> <path_to_text>`